
#ifndef __SWATCH_DUMMY_DUMMYDEVICE_HPP__
#define __SWATCH_DUMMY_DUMMYDEVICE_HPP__


#include "swatch/action/Device.hpp"
#include "swatch/dummy/DummyDeviceController.hpp"


namespace swatch {
namespace dummy {

class DummyDeviceController;


class DummyDevice : public action::Device {
public:
  DummyDevice(const swatch::core::AbstractStub& aStub);
  virtual ~DummyDevice();

  DummyDeviceController& getController()
  {
    return mController;
  }

  void retrieveMetricValues() {}

private:
  DummyDeviceController mController;
};


} // namespace dummy
} // namespace swatch

#endif /* SWATCH_DUMMY_DUMMYDEVICE_HPP */
