
#ifndef __SWATCH_DUMMY_DUMMYDEVICECONTROLLER_HPP__
#define __SWATCH_DUMMY_DUMMYDEVICECONTROLLER_HPP__


#include <string>


namespace swatch {
namespace dummy {


class DummyDeviceController {
public:
  DummyDeviceController(const std::string& aURI, const std::string& aAddrTable);

  ~DummyDeviceController();

  void program();

  void reboot();

  void reset();

  void configureClocks();

  void configureRxMGTs();

  void configureTxMGTs();

  void alignLinks();
};


} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_DUMMYDEVICECONTROLLER_HPP__ */
