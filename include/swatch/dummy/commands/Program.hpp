
#ifndef __SWATCH_DUMMY_COMMANDS_PROGRAM_HPP__
#define __SWATCH_DUMMY_COMMANDS_PROGRAM_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class Program : public action::Command {
public:
  Program(const std::string& aId, action::ActionableObject& aActionable);
  ~Program();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_PROGRAM_HPP__ */
