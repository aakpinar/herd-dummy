
#ifndef __SWATCH_DUMMY_COMMANDS_RESET_HPP__
#define __SWATCH_DUMMY_COMMANDS_RESET_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class Reset : public action::Command {
public:
  Reset(const std::string& aId, action::ActionableObject& aActionable);
  ~Reset();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_RESET_HPP__ */
