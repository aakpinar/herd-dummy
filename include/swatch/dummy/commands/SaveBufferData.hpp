
#ifndef __SWATCH_DUMMY_COMMANDS_SAVEBUFFERDATA_HPP__
#define __SWATCH_DUMMY_COMMANDS_SAVEBUFFERDATA_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class SaveBufferData : public action::Command {
public:
  SaveBufferData(const std::string& aId, action::ActionableObject& aActionable);
  ~SaveBufferData();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_SAVEBUFFERDATA_HPP__ */
