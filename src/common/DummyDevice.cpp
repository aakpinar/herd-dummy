
#include "swatch/dummy/DummyDevice.hpp"


#include "swatch/action/StateMachine.hpp"
#include "swatch/core/Factory.hpp"
#include "swatch/dummy/DummyDeviceController.hpp"
#include "swatch/dummy/commands/AlignLinks.hpp"
#include "swatch/dummy/commands/ConfigureRxMGTs.hpp"
#include "swatch/dummy/commands/ConfigureTxMGTs.hpp"
#include "swatch/dummy/commands/Program.hpp"
#include "swatch/dummy/commands/Reboot.hpp"
#include "swatch/dummy/commands/Reset.hpp"
#include "swatch/dummy/commands/SaveBufferData.hpp"


SWATCH_REGISTER_CLASS(swatch::dummy::DummyDevice)


namespace swatch {
namespace dummy {


DummyDevice::DummyDevice(const core::AbstractStub& aStub) :
  Device(aStub),
  mController(getStub().uri, getStub().addressTable)
{
  // Register commands
  auto& program = registerCommand<commands::Program>("program");
  auto& reboot = registerCommand<commands::Reboot>("reboot");
  auto& reset = registerCommand<commands::Reset>("reset");
  auto& cfgTxMGTs = registerCommand<commands::ConfigureTxMGTs>("configureTxMGTs");
  auto& cfgRxMGTs = registerCommand<commands::ConfigureRxMGTs>("configureRxMGTs");
  auto& alignLinks = registerCommand<commands::AlignLinks>("alignLinks");
  registerCommand<commands::SaveBufferData>("saveBufferData");

  // Register FSMs
  action::StateMachine& fsm = registerStateMachine("myFSM", "Initial", "Error");
  fsm.addState("Configured");
  fsm.addState("Aligned");

  fsm.addTransition("hardReset", "Initial", "Initial").add(reboot).add(program);

  action::Transition& configure = fsm.addTransition("configure", "Initial", "Configured");
  configure.add(reset);
  configure.add(cfgTxMGTs);

  auto& align = fsm.addTransition("align", "Configured", "Aligned");
  align.add(cfgRxMGTs);
  align.add(alignLinks);
}


DummyDevice::~DummyDevice()
{
}


} // namespace dummy
} // namespace swatch
