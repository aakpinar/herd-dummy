#include "swatch/dummy/DummyDeviceController.hpp"


namespace swatch {
namespace dummy {


DummyDeviceController::DummyDeviceController(const std::string& aURI, const std::string& aAddrTable)
{
}


DummyDeviceController::~DummyDeviceController()
{
}


void DummyDeviceController::program()
{
}


void DummyDeviceController::reboot()
{
}


void DummyDeviceController::reset()
{
}



void DummyDeviceController::configureClocks()
{
}


void DummyDeviceController::configureRxMGTs()
{
}


void DummyDeviceController::configureTxMGTs()
{
}


void DummyDeviceController::alignLinks()
{
}

}
}
