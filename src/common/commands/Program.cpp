
#include "swatch/dummy/commands/Program.hpp"


#include <chrono>
#include <thread>

#include "swatch/action/File.hpp"
#include "swatch/dummy/DummyDevice.hpp"


namespace swatch {
namespace dummy {
namespace commands {

using namespace std::string_literals;


Program::Program(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, "Dummy command's default result!"s)
{
  registerParameter<action::File>("package", { "/path/to/package.tgz", "package.tgz" });
  registerParameter("cmdDuration", uint32_t(5));
  registerParameter<bool>({"returnWarning"s, "Force dummy command to return warning"}, false);
  registerParameter<bool>({"returnError"s, "Force dummy command to return error"}, false);
  registerParameter<bool>({"throw"s, "Force dummy command to throw"}, false);
}

Program::~Program()
{
}

action::Command::State Program::code(const core::ParameterSet& aParams)
{
  DummyDeviceController& lController = getActionable<DummyDevice>().getController();

  if (aParams.get<bool>("throw"))
    throw core::RuntimeError("An exceptional error occurred!");

  State lState = kDone;
  if (aParams.get<bool>("returnError"))
    lState = kError;
  else if (aParams.get<bool>("returnWarning"))
    lState = kWarning;
  else
    lController.program();


  const size_t lNrSeconds = aParams.get<uint32_t>("cmdDuration");
  for (size_t i = 0; i < (lNrSeconds * 4); i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(250));
    std::ostringstream lMsg;
    lMsg << "Done " << i + 1 << " of " << (lNrSeconds * 4) << " things";
    setProgress(float(i) / float(lNrSeconds * 4), lMsg.str());
  }

  return lState;
}


} // namespace commands
} // namespace dummy
} // namespace swatch
