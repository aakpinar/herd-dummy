
#include "swatch/dummy/commands/SaveBufferData.hpp"


#include <chrono>
#include <fstream>
#include <thread>

#include "swatch/action/File.hpp"
#include "swatch/dummy/DummyDevice.hpp"


namespace swatch {
namespace dummy {
namespace commands {

using namespace std::string_literals;


SaveBufferData::SaveBufferData(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, "", "Capture data from I/O buffers and write to file", aActionable, "Dummy command's default result!"s)
{
  // Extra dummy parameters
  registerParameter("cmdDuration", uint32_t(5));
  registerParameter<bool>({"returnWarning"s, "Force dummy command to return warning"}, false);
  registerParameter<bool>({"returnError"s, "Force dummy command to return error"}, false);
  registerParameter<bool>({"throw"s, "Force dummy command to throw"}, false);
  registerParameter({"size"s, "Size of returned file (kByte)"}, uint32_t(5));
}

SaveBufferData::~SaveBufferData()
{
}

action::Command::State SaveBufferData::code(const core::ParameterSet& aParams)
{
  DummyDeviceController& lController = getActionable<DummyDevice>().getController();

  if (aParams.get<bool>("throw"))
    throw core::RuntimeError("An exceptional error occurred!");

  State lState = kDone;
  if (aParams.get<bool>("returnError"))
    lState = kError;
  else if (aParams.get<bool>("returnWarning"))
    lState = kWarning;
  else
    lController.configureRxMGTs();


  const std::string lFileName("dummy.dat");
  std::fstream lFile;
  lFile.open(lFileName.c_str(), std::ios::out | std::ios::app);
  if (lFile.fail())
    throw std::runtime_error("Failed to open file '" + lFileName + "'");

  for (size_t i = 0; i < aParams.get<uint32_t>("size"); i++) {
    lFile << " ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ \n";
    for (size_t j = 0; j < 31; j++) {
      lFile << "0 1 2 3 4 5 6 7 8 9 a b c d e\n";
    }
  }

  lFile.flush();
  lFile.close();

  setResult( boost::any(swatch::action::File(lFileName, lFileName)) );


  const size_t lNrSeconds = aParams.get<uint32_t>("cmdDuration");
  for (size_t i = 0; i < (lNrSeconds * 4); i++) {
    std::this_thread::sleep_for(std::chrono::milliseconds(250));
    std::ostringstream lMsg;
    lMsg << "Done " << i + 1 << " of " << (lNrSeconds * 4) << " things";
    setProgress(float(i) / float(lNrSeconds * 4), lMsg.str());
  }

  return lState;
}


} // namespace commands
} // namespace dummy
} // namespace swatch
